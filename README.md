
s<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://kshrdalumni.herokuapp.com/swagger-ui/index.html">
    <img src="http://34.142.199.24:8090/images/akolfqGroup%2044.png" alt="Logo" width="400" height="80">
  </a>
</p>
<h3 align="center">HealthChains</h3>

  <p align="center">
    HealthChains is the first health care service in cambodia base blockchain technology.
    <br />
    <br />
    <a href="https://kshrdalumni.herokuapp.com/swagger-ui/index.html">View Demo</a>
  </p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a>
    </li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
<li><a href="#what-we-have-done">What we have done</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

### Built With

- [SDK]()
- [Maven](https://maven.apache.org/)
- [Postgresql](https://www.postgresql.org/)
- [Smart Constract](https://www.elastic.co/)

<!-- GETTING STARTED -->

## Getting Started

### Installation
### NOTE: JDK 11 required for all installation methods

## Usage

## Steps to test API
### [Prerequired] 
 - Must be up network (HealthChains infra )
 - Make sure no peer or container is Exit
 - Base on the blockchain technology to perform any task on our API you must have specific identity
### [Admin Account]
  - username: admin
  - password: adminpw

-----
RestController

### [AuthController](http://34.142.199.24:8090/swagger-ui/index.html#/auth-rest-controller)
- Step 01 -> [Register](http://34.142.199.24:8090/swagger-ui/index.html#/auth-rest-controller/registerNewUser)
- Optoinal -> [RegisterWithOutNetwork](http://34.142.199.24:8090/swagger-ui/index.html#/auth-rest-controller/registerNewUserTest) **Note:** This is use to register without network which mean this user don't have any permission to work in the network This route is use for testing in development mod only
- Step 02 -> [Login](http://34.142.199.24:8090/swagger-ui/index.html#/auth-rest-controller/login)


### [AdminRestController](http://34.142.199.24:8090/swagger-ui/index.html#/admin-rest-controller)
- Step 03 -> [RegisterDoctor](http://34.142.199.24:8090/swagger-ui/index.html#/admin-rest-controller/RegisterDoctor)
- Step 04 -> [Add new doctor's education by username](http://34.142.199.24:8090/swagger-ui/index.html#/admin-rest-controller/addNewStudy) 
- Step 05 -> [Add new doctor's experience by username](http://34.142.199.24:8090/swagger-ui/index.html#/admin-rest-controller/addNewExperience)
- Step 06 -> [UpdateDoctorByUsername](http://34.142.199.24:8090/swagger-ui/index.html#/admin-rest-controller/UpdateDoctor)
- Step 05 -> [GetAllUsers](http://34.142.199.24:8090/swagger-ui/index.html#/admin-rest-controller/GetAllUsers)
- Step 06 -> [Ban user account by id](http://34.142.199.24:8090/swagger-ui/index.html#/admin-rest-controller/BanUser) **Note:** id = user id type interger in database Example 219 , Status type boolean 
Notice: Only using admin account that can register doctor


### [Doctor Account](http://34.142.199.24:8090/swagger-ui/index.html#/doctor-controller)
- Step 07 -> [create record by user virtualIdCard](http://34.142.199.24:8090/swagger-ui/index.html#/doctor-controller/createRecord)  **Note:** idCard = virtual_id_card Example: hc_503066654
- Step 08 -> [update recordB by RecordId](http://34.142.199.24:8090/swagger-ui/index.html#/doctor-controller/updateRecord) **Note:* id = record id, recordToken is the same . Example: krisna-377321162787402927
- Step 09 -> [search patient by virtualIdCard](http://34.142.199.24:8090/swagger-ui/index.html#/doctor-controller/Search%20patient%20by%20virtualID) 
- Step 10 -> [get user information that doctor used to created record](http://34.142.199.24:8090/swagger-ui/index.html#/doctor-controller/getUserByDoctor)
- Step 12 -> [request to view patient record by virtualIdCard](http://34.142.199.24:8090/swagger-ui/index.html#/doctor-controller/requested%20to%20view%20patient%20record%20) 
- Step 13 -> [get all user that doctor have request](34.142.199.24:8090/swagger-ui/index.html#/doctor-controller/get requested by doctor id) 
- Step 14 -> [get record by record id after patient approved](http://34.142.199.24:8090/swagger-ui/index.html#/doctor-controller/view%20record%20by%20patient%20virtualIdCard%20after%20approve)  **Note:* virtualIdCard= patientVirtualIdCard Example: hc_503066654, record id the same record Token Example: krisna-377321162787402927 This route will reture object of record that have status:true (public)
- Step 15 -> [get all patient record after approve by virtualIdCard](http://34.142.199.24:8090/swagger-ui/index.html#/doctor-controller/view%20all%20record%20by%20patient%20virtualIdCard%20after%20approve) **Note:* virtualIdCard= patientVirtualIdCard This route will record array object of patient record that have status true (public)


### [UserController](http://34.142.199.24:8080/swagger-ui/index.html#/user-controller)
- Step 16 -> [cGetProfileByUsername](http://34.142.199.24:8090/swagger-ui/index.html#/user-controller/GetProfileByUsername)
- Step 17 -> []()
- Step 18 -> []()
- Step 19 -> []()
- Step 20 -> []()
- Step 21 -> []()
- Step 22 -> []()
- Step 23 -> []()
- Step 24 -> []()


### [ReviseController](http://localhost:8083/swagger-ui/index.html#/revise-rest-controller)
- Step 07 -> [get all revise user profile](http://localhost:8083/swagger-ui/index.html#/revise-rest-controller/geReviseProfile)
- Step 08 -> [get revise profile by uuid](http://localhost:8083/swagger-ui/index.html#/revise-rest-controller/getReviseByUuid)

### [TemplateController](http://localhost:8083/swagger-ui/index.html#/template-rest-controller/)
- Step 09 -> [ get all templates](http://localhost:8083/swagger-ui/index.html#/template-rest-controller/getAllTemplate)
- Step 10 -> [ find template by uuid](http://localhost:8083/swagger-ui/index.html#/template-rest-controller/findTemplate)

### [FileController](http://localhost:8083/swagger-ui/index.html#/file-storage-rest-controller/)
- Step 11 -> [ upload single file](http://localhost:8083/swagger-ui/index.html#/file-storage-rest-controller/uploadFile)
- Step 12 -> [ upload multiple files](http://localhost:8083/swagger-ui/index.html#/file-storage-rest-controller/uploadMultipleFiles)
  **Noted:** It's not work on swagger but working fine on **postman** and **react**
- Step 13 -> [ download by file name](http://localhost:8083/swagger-ui/index.html#/file-storage-rest-controller/downloadFile)

### [AccountController](http://localhost:8083/swagger-ui/index.html#/account-controller/sendEmail)
- Step 14 -> [send verify code to user's email](http://localhost:8083/swagger-ui/index.html#/account-controller/sendEmail)

### [FilterController](http://localhost:8083/swagger-ui/index.html#/filter-rest-controller/)
- Step 15 -> [ filter by username](http://localhost:8083/swagger-ui/index.html#/filter-rest-controller/getUserProfileByUsername)
- Step 16 -> [ filter by skill](http://localhost:8083/swagger-ui/index.html#/filter-rest-controller/getUserProfileBySkill)
- Step 17 -> [ filter by generation](http://localhost:8083/swagger-ui/index.html#/filter-rest-controller/getUserProfileByGeneration)

### [AdminController](http://localhost:8083/swagger-ui/index.html#/admin-rest-controller/)
Account for admin:
**Email**: sangsokea109@gmail.com **password**: 1234567890
- Step 18 -> [get all authUser ](http://localhost:8083/swagger-ui/index.html#/admin-rest-controller/getUserProfile_1)
- Step 19 -> [disable or enable user by userId](http://localhost:8083/swagger-ui/index.html#/admin-rest-controller/disableAccount)
- Step 20 -> [update user credential](http://localhost:8083/swagger-ui/index.html#/admin-rest-controller/updateUser)
- Step 21 -> [delete user profile](http://localhost:8083/swagger-ui/index.html#/admin-rest-controller/deleteUserProfileByUuid)
- Step 22 -> [get list user profile](http://localhost:8083/swagger-ui/index.html#/admin-rest-controller/getUserProfiles)

### [Bonus: Elasticsearch (dynamic search)](http://18.142.237.124:9200/person/_search?pretty=true&q=*SearchKey*&from=0&size=3)
```http
  GET http://18.142.237.124:9200/person/_search?pretty=true&q=*SearchKey*&from=0&size=3
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `q`      | `string` | key word to search |
| `from`      | `integer` | start from number (row) |
| `size`      | `integer` | end in number |

### **Elasticsearch** credential:
- #### Username: elastic
- #### Password: KdpvxS9iTZS2doJkCBXo

_For more examples, please refer to
the [service Demo](https://kshrdalumni.herokuapp.com/swagger-ui/index.html)

Account for admin:
Email: sangsokea109@gmail.com password: 1234567890

## What we have done

- [AuthController](http://localhost:8083/swagger-ui/index.html#/auth-rest-controller/)
- [UserController](http://localhost:8083/swagger-ui/index.html#/user-rest-controller/)
- [ReviseController](http://localhost:8083/swagger-ui/index.html#/revise-rest-controller)
- [AdminController](http://localhost:8083/swagger-ui/index.html#/admin-rest-controller/)
- [TemplateController](http://localhost:8083/swagger-ui/index.html#/auth-rest-controller/)
- [FileController](http://localhost:8083/swagger-ui/index.html#/file-storage-rest-controller/)
- [AccountController](http://localhost:8083/swagger-ui/index.html#/account-controller/sendEmail)
- [FilterController](http://localhost:8083/swagger-ui/index.html#/filter-rest-controller/)
- [Bonus: Elasticsearch (dynamic search)](http://18.142.237.124:9200/person/_search?pretty=true&q=*SearchKey*&from=0&size=3)



